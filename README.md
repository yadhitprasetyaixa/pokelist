# PokeList

Pokelist merupakan website yang menyediakan daftar pokemon beserta dengan berbagai kelengkapan lainnya. Data diambil dari API [pokeapi.co](https://pokeapi.co/docs/v2) dengan data akurat, kecuali untuk seri **Pokemon : Sword and Shield**

## Fitur saat ini

### Pokelist

Merupakan halaman utama dari website ini. Halaman ini memuat daftar pokemon yang terdapat di pokemon universe, lebih kurang 1279 data. Setiap data pokemon dapat dipilih untuk mengakses detail lebih lengkap.

### Pokepage

Merupakan halaman detail informasi dari pokemon yang dipilih melalui halaman Pokelist. Halaman ini memuat detail pokemon, seperti ilustrasi, weight, height, types, ability, stat dan moves. 

## Teknologi yang digunakan

- React.Js : Digunakan sebagai framework dasar untuk membangun sisi *front-end* website
- Bootstrap : Digunkanan untuk *styling* dari elemen UI yang ditampilkan 

## Cara menjalankan aplikasi
1. **Unduh code ke direktori lokal**, dapat dilakukan dengan melakukan clone terhadap [repositori aplikasi](https://gitlab.com/yadhitprasetyaixa/pokelist)
2. **Install dependency yang dibutuhkan**, dapat dilakuakn dengan menjalankan command `npm install` pada terminal di direktori lokal aplikasi. Untuk detail dari dependency yang akan dilakukan dapat di cek pada file **package.json**. Pastikan anda sudah memiliki instalasi Node.Js untuk bisa menjalankan aplikasi ini pada sistem
3. **Jalankan aplikasi**, dapat dilakukan dengan memasukan command `npm start` dan aplikasi akan terbuka pada browser preferensi anda dengan alamat [localhost:3000](http://localhost:3000). **Pastikan port yang digunakan (port 3000) belum digunakan oleh service lainnya.** Jika port telah digunakan silahkan atur port yang tersedia memasukan command `$env:PORT={N}` dengan `N` mewakili post yang diinginkan. Lebih lanjut dapat dilihat [disini](https://scriptverse.academy/tutorials/reactjs-change-port-number.html#:~:text=In%20ReactJS%2C%20the%20easiest%20way,the%20port%20number%20to%205000.&text=your%20local%20server%20will%20run%20on%20port%205000)
4. Untuk mematikan aplikasi dapat dilakuakan dengan menekan tombol <kbd>Ctrl</kbd> + <kbd>C</kbd> pada terminal yang menjalankan aplikasi
