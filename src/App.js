import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useState } from 'react';
import './App.css';
import Navbar from './components/Navbar';
import Pokelist from './components/Pokelist';
import Pokepage from './components/Pokepage';

function App() {
  const [url, setUrl] = useState("https://pokeapi.co/api/v2/pokemon/?limit=18")
  const [next, setNext] = useState("")
  const [prev, setPrev] = useState("")
  const [page, setPage] = useState(1)


  return (
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Navbar />}>
            <Route index element={<Pokelist url={url} setter={setUrl} page={page} setPage={setPage} next={next} setNext={setNext} prev={prev} setPrev={setPrev}/>} />
            <Route path="pokepage/:name" element={<Pokepage />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
