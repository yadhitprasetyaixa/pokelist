import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import "./Pokepage.css"

const Pokepage = () => {
    const loc = useLocation()
    const [name, setName] = useState("")
    const [weight, setWeight] = useState("")
    const [height, setHeight] = useState("")
    const [types, setTypes] = useState([])
    const [abils, setaAbils] = useState([])
    const [stats, setStats] = useState([])
    const [moves, setMoves] = useState([])
    const pokemon = loc.pathname.substring(loc.pathname.lastIndexOf('/') + 1)
    const [id,setId] = useState("")
    const pic = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + id + ".png"
    const url = "https://pokeapi.co/api/v2/pokemon/" + pokemon

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(url);
                const poke = await response.json();
                // console.log(json);
                setId(poke.id)
                setName(poke.name)
                setHeight(poke.height)
                setWeight(poke.weight)
                setTypes(poke.types)
                setaAbils(poke.abilities)
                setStats(poke.stats)
                setMoves(poke.moves)
                } catch (error) {
                console.log("hey", error);
                }
        };
        fetchData();

    }, [])

    return(
        <div className="row pt-4">
            <div className="col card p-3 mb-5">
                <div className="row">
                    <div className="col-md-4 col-xl-3 col-12">
                        <img src={pic} className="card-img-top img-card" />
                        <h3 className="card-title">{name.charAt(0).toUpperCase() + name.slice(1)}</h3>
                        <table className="table">
                            <thead>
                                <tr className="table-warning">
                                    <th scope="col-6">Height</th>
                                    <th scope="col-6">Weight</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        {height*10} cm
                                    </td>
                                    <td>   
                                        {weight/10} Kg
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h5>Types</h5>
                        <table className="table">
                            <tbody>
                                <tr className="table-warning">
                                {types.map(type => <td>{type.type.name.charAt(0).toUpperCase() + type.type.name.slice(1)}</td>)}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="col-md-8 col-xl-9 col-12 card-body">
                        <h3 className="card-title d-none d-xl-block">{name.charAt(0).toUpperCase() + name.slice(1)}</h3>
                        <div className="row">
                            <div className="col-md-6 col-12">
                                <h5>Ability</h5>
                                <table className="table">
                                    <thead>
                                        <tr className="table-warning">
                                            <th scope="col-11">Moves</th>
                                            <th scope="col-1">Slot</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {abils.map(abil => 
                                        <tr>
                                            <td>{abil.ability.name}</td>
                                            <td>{abil.slot}</td>
                                        </tr>
                                        )}
                                    </tbody>
                                </table>
                                <h5>Stat</h5>
                                <table className="table">
                                    <thead>
                                        <tr className="table-warning">
                                            <th scope="col-6">Stat</th>
                                            <th scope="col-6">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {stats.map(stat => 
                                        <tr>
                                            <td>{stat.stat.name}</td>
                                            <td>{stat.base_stat}</td>
                                        </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-md-6 col-12">
                                <div>
                                    <h5>Moves</h5>
                                    <table className="table mb-0" >
                                        <thead>
                                            <tr className="table-warning">
                                                <th>Moves Name</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div className="overflow-y-scroll" style={{maxHeight:"49vh"}}>
                                    <table className="table">
                                        <tbody className="overflow-y-scroll vh-50 w-100">
                                        {moves.map(move => 
                                            <tr>
                                                <td className="">{move.move.name}</td>
                                            </tr>
                                            )}
                                        </tbody>
                                    </table>
                                </div>
                                
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Pokepage