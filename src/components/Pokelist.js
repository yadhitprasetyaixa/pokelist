import { useEffect, useState } from "react"
import { Link } from "react-router-dom";
import Pokecard from "./Pokecard"

const Pokelist = (props) => {
    const [list, setList] = useState([]);
    const next = props.next
    const setNext = props.setNext
    const prev = props.prev
    const setPrev = props.setPrev
    const url = props.url
    const setUrl = props.setter
    const page = props.page
    const setPage = props.setPage


    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(url);
                const poke = await response.json();
                // console.log(json);
                setNext(poke.next)
                setPrev(poke.previous)
                setList(poke.results);
                } catch (error) {
                console.log("hey", error);
                }
        };
        fetchData();
        console.log(next + "hey")

    }, [page])
    
    return(
        <div className="pt-4">
            <div className="row g-4">
                {list.map(poke => <Link className="btn col-6 col-md-4 col-xl-2" to={"pokepage/" + poke.name}><Pokecard pokename={poke.name} data={poke.url} /></Link>)}
            </div>
            <div className="row justify-content-center my-5 px-3">
                <div className="col-md-3" />
                <button className="col-md-2 col-5 btn btn-warning" disabled={page<=1} onClick={() => [setPage(page - 1), setUrl(prev)]}>Previous</button>
                <div className="col-md-2 col-2"><h3>{page}</h3></div>
                <button className="col-md-2 col-5 btn btn-warning" onClick={() => [setPage(page + 1), setUrl(next)]}>Next</button>
                <div className="col-md-3" />
            </div>
            
        </div>
    )
}

export default Pokelist