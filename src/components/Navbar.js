import { Link, Outlet } from "react-router-dom"

const Navbar = (props) => {
    return(
        <div>

            <nav className="navbar bg-warning fixed-top">
                <div className="container-fluid">
                    <Link to={"/"} className="navbar-brand">Pokelist</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="offcanvas offcanvas-start" tabIndex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
                        <div className="offcanvas-header">
                            <h5 className="offcanvas-title" id="offcanvasNavbarLabel">Pokelist</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                        <div className="offcanvas-body">
                            <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
                                <li className="nav-item">
                                    <Link to="/" className="nav-link active" aria-current="page">Pokelist</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div className="App container-xl px-3">
                <Outlet />
            </div>
        </div>
    )
}

export default Navbar