import "./Pokecard.css"

const Pokecard = (props) => {
    const pokename = props.pokename
    var data = props.data
    if (data != null) {
        
        data = data.slice(34).replace("/","")
    }
    const url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + data + ".png"

    return(
        <div className="col">
            <div className="card box">
                <img src={url} className="card-img-top" />
                <div className="card-body">
                    <p className="card-text">{pokename.charAt(0).toUpperCase() + pokename.slice(1)}</p>
                </div>
            </div>
        </div>
    )
}

export default Pokecard